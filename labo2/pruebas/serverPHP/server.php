<?php
include("lib/constantes.php");
include("persistencia/Conexion.php");
include("lib/funciones.php");
//Notificar todos los errores de PHP
error_reporting(E_ALL);
//Permite al script esperar más conexiones.
set_time_limit(0);
/*Activar volcado de salida implícito
para ver lo que se obtiene mientras llega*/
ob_implicit_flush();

echo "[...] CREANDO SOCKET\n";

//Se crea el socket
if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "socket_create() falló: razón: " . socket_strerror(socket_last_error()) . "\n";
}
//Se vincula la dirección IP y puerto especificados al socket.
if (socket_bind($sock, ADDRESS, PORT) === false) {
    echo "socket_bind() falló: razón: " . socket_strerror(socket_last_error($sock)) . "\n";
}
//El socket comienza a escuchar.
if (socket_listen($sock, 1) === false) {
    echo "socket_listen() falló: razón: " . socket_strerror(socket_last_error($sock)) . "\n";
}

echo "[!] SOCKET CREADO, ESCUCHANDO EN ".ADDRESS.":".PORT."\n\n\n";
$ultima_fecha = "1000-01-01 00:00:00";
do {
    //Se espera por nuevas conexiones.
    echo "[...] ESPERANDO NUEVAS CONEXIONES\n\n";
    if(($msjsock = socket_accept($sock)) === false) {
        echo "socket_accept() falló: razón: " . socket_strerror(socket_last_error($sock)) . "\n";
        break;
    }
    echo "[!] NUEVA CONEXIÓN DETECTADA\n";

    // Se crea y abre conexión a base de datos.
    $conexion = new Conexion();
    try {
        $conexion->abrir();
    } catch (Exception $e) {
        echo "EXCEPCIÓN CAPTURADA: " . $e->getMessage() . "\n";
    }
    // Declaración de variable string que tendrá formato csv.
    $csvString = '';
    $result = $conexion->obtenerDatosMayorAFecha($ultima_fecha);
    if ($result) {
        if($result->num_rows > 0){
            while ($row = $result->fetch_row()) {
                $csvString .= str_putcsv($row);
                $csvString .= "@";
            }
            $csvString .= "\n\n";
             if(! $arrayUlt_fecha = $conexion->obtenerUltimaFecha()->fetch_row()) {
                 echo "ERROR EN obtenerUltimaFecha() en server.php.";
             }
             $ultima_fecha = $arrayUlt_fecha[0];
             echo $ultima_fecha;
             
        } else {
            $csvString = "empty\n\n";
        }
        $result->close();
    } else {
        echo "ERROR obtenerDatosMayorAFecha() Falló en el archivo server.php.\n";
    }
    socket_write($msjsock, $csvString, strlen($csvString));
    echo "[>] DATOS ENVIADOS\n";

    socket_close($msjsock);
    echo "[!] CONEXIÓN CERRADA\n\n";
} while (true);
echo "[...] CERRANDO SERVER, ADIOS!\n";
socket_close($sock);
exit();
?>