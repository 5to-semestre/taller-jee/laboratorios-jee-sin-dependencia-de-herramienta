<?php
    class Conexion {
        private $conexion;

        private function conectar() {
            $this->conexion = new mysqli(SERVIDOR, USUARIO, CONTRASENA, BD) or die ("ERROR DE LA CONEXIÓN");
            if($this->conexion->connect_errno) {
                throw new Exception(
                    $this->conexion->connect_error." - Nro de error: ".$this->conexion->connect_errno
                );
            }
            // return $this->conexion;
        }
    
        public function abrir() {
            $this->conectar();
        }
    
        public function cerrar() {
            $this->conexion->close();
        }
    
        private function consultaSql($sql, $resultmode = MYSQLI_STORE_RESULT) {
            return $this->conexion->query($sql, $resultmode);
        }
        
        public function cantRegistros($sql) {
            return $this->consultaSql($sql)->num_rows;
        }
        // Si retorna 0, no existe.
        // Si retorna mayor a 0, existe.
        public function existeDato($id) {
            $sql = "SELECT id FROM datos_acumulados WHERE id = ".$id.";";
            return $this->cantRegistros($sql);
        }

        public function obtenerDatosMayorAFecha($datetime = "1000-01-01 00:00:00") {
            $sql = "SELECT id, fecha_ingresado, NombreSistema, Plataforma, SO, UsoCPU, UsoRAM FROM datos_acumulados WHERE fecha_ingresado > '".$datetime."';";
            return $this->consultaSql($sql);
        }

        public function obtenerUltimaFecha() {
            $sql="SELECT fecha_ingresado FROM datos_acumulados WHERE fecha_ingresado = (SELECT MAX(fecha_ingresado) FROM datos_acumulados);";
            return $this->consultaSql($sql);
        }
    }
?>