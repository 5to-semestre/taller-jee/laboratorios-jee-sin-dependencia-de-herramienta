<?php
if(!function_exists('str_putcsv'))
{
    function str_putcsv($input, $delimiter = ',', $enclosure = '"')
    {
        // Abrir un "archivo" en memoria para escritura/lectura.
        $fp = fopen('php://temp', 'r+');
        // Escribir el arreglo $input en el "archivo" usando fputcsv().
        fputcsv($fp, $input, $delimiter, $enclosure);
        // Posicionar nuevamente al inicio del "archivo" para 
        // poder leer lo que acabamos de escribir.
        rewind($fp);
        // Leer toda la línea en una variable.
        $data = fread($fp, 9999999);
        // Cerrar el "archivo".
        fclose($fp);
        // Se retorna la data eliminando posibles espacios.
        return rtrim($data, "\n");
    }
}
?>