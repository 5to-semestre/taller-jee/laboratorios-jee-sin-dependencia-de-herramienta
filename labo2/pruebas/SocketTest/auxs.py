import socket 
import threading
import time
from datetime import datetime
import socket
import os 
import sys
import platform
import psutil
import csv

HEADER = 64
PORT = 5050
SERVER = "127.0.0.1"
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
EMPAQUETADO = []


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    while connected:
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        for line in open("deposited.csv"):
            EMPAQUETADO.append(line)
            EMPAQUETADO.append('|')
        enviado = str(EMPAQUETADO)
        conn.send(enviado.encode(FORMAT))
        connected = False
    conn.close()
        

def start():
    server.listen()
    print(f"[LISTENING] Server is listening on {SERVER}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")


print("[STARTING] server is starting...")
start()