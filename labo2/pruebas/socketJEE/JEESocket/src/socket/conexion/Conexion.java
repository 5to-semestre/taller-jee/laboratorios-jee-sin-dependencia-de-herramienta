package socket.conexion;

import java.io.*;
import java.net.*;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import model.Dato;
import persistencia.dao.DatoDAO;

public class Conexion {

	private static String HOST = "127.0.0.1";
	private static int PUERTO = 5051;

	public static void main(String[] args) throws InterruptedException {
		Socket socket = null;
		BufferedReader recibido = null;
		System.out.println("Bienvenido al cliente Socket!");
		while (true) {
			try {
				System.out.println("[...] CREANDO CLIENTE");

				socket = new Socket(HOST, PUERTO);
				recibido = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				System.out.println("[!] CONECTADO CON " + HOST + ":" + PUERTO);

				String msg = recibido.readLine();

				System.out.println("[!] DATOS RECIBIDOS");
				
				if(msg.equals("empty")) {
					System.out.println("[!] NO SE RECIBIERON NUEVOS REGISTROS.");
				} else {

					ArrayList<String> listaLineas = new ArrayList<String>(Arrays.asList(msg.split("@")));
	
					for (String linea : listaLineas) {
						// ARRAY CARGADO CON LOS DATOS EN EL ÓRDEN QUE ESTÁN EN LA BASE
						// DE DATOS DEL SERVER PHP.
						ArrayList<String> datos = new ArrayList<String>(Arrays.asList(linea.split(",")));
						Dato objDato = new Dato();
						objDato.setID(Integer.parseInt(datos.get(0)));
						int finCadenaFecha = datos.get(1).indexOf(" ");
						objDato.setFecha_hora(Date.valueOf(datos.get(1).substring(1, finCadenaFecha - 1)));
						objDato.setNombreSistema(datos.get(2));
						objDato.setPlataforma(datos.get(3));
						objDato.setSO(datos.get(4));
						objDato.setUsoCPU(datos.get(5));
						objDato.setUsoRAM(datos.get(6));
	
						DatoDAO datoDAO = new DatoDAO();
						try {
							datoDAO.guardarDato(objDato);
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					System.out.println("[!] DATOS CARGADOS EN BASE DE DATOS.");
				}
				System.out.println("[...] CERRANDO CLIENTE, ADIÓS!");
				if (socket != null)
					socket.close();
			} catch (UnknownHostException e) {
				System.out.println("El host no existe o no está activo");
			} catch (IOException e) {
				System.out.println("Error de entrada/salida.");
			}
			Thread.sleep(10000);
		}

	}

}
