package model;

import java.sql.Date;

public class Dato {
	private int ID;
	private Date fecha_hora;
	private String nombreSistema;
	private String plataforma;
	private String SO;
	private String usoCPU;
	private String usoRAM;

	public Dato() {
		ID = -1;
		this.fecha_hora = null;
		this.nombreSistema = null;
		this.plataforma = null;
		SO = null;
		this.usoCPU = null;
		this.usoRAM = null;
	}

	public Dato(int iD, Date fecha_hora, String nombreSistema, String plataforma, String sO, String usoCPU,
			String usoRAM) {
		super();
		ID = iD;
		this.fecha_hora = fecha_hora;
		this.nombreSistema = nombreSistema;
		this.plataforma = plataforma;
		SO = sO;
		this.usoCPU = usoCPU;
		this.usoRAM = usoRAM;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Date getFecha_hora() {
		return fecha_hora;
	}

	public void setFecha_hora(Date fecha_hora) {
		this.fecha_hora = fecha_hora;
	}

	public String getNombreSistema() {
		return nombreSistema;
	}

	public void setNombreSistema(String nombreSistema) {
		this.nombreSistema = nombreSistema;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public String getSO() {
		return SO;
	}

	public void setSO(String sO) {
		SO = sO;
	}

	public String getUsoCPU() {
		return usoCPU;
	}

	public void setUsoCPU(String usoCPU) {
		this.usoCPU = usoCPU;
	}

	public String getUsoRAM() {
		return usoRAM;
	}

	public void setUsoRAM(String usoRAM) {
		this.usoRAM = usoRAM;
	}
}
