package persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Dato;
import persistencia.conexion.Conexion;

public class DatoDAO {
	private Connection connection;
	private PreparedStatement statement;
	private boolean estadoOperacion;
	
	private Connection obtenerConexion() throws SQLException {
		return Conexion.getConnection();
	}
	
	public boolean guardarDato(Dato dato) throws SQLException {
		String sql = null;
		estadoOperacion = false;
		connection = obtenerConexion();

		try {
			connection.setAutoCommit(false);
			sql = "INSERT INTO `DATOS`(`ID`, `fecha_hora`, `nombreSistema`, `plataforma`, `SO`, `usoCPU`, `usoRAM`) VALUES (?,?,?,?,?,?,?);";
			statement = connection.prepareStatement(sql);

			statement.setString(1, null);
			statement.setDate(2, dato.getFecha_hora());
			statement.setString(3, dato.getNombreSistema());
			statement.setString(4, dato.getPlataforma());
			statement.setString(5, dato.getSO());
			statement.setString(6, dato.getUsoCPU());
			statement.setString(7, dato.getUsoRAM());
			estadoOperacion = statement.executeUpdate() > 0;

			connection.commit();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			connection.rollback();
			e.printStackTrace();
		}

		return estadoOperacion;
	}
}