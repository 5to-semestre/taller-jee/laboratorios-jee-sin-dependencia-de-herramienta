package socketsjee.taller.controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import socketsjee.taller.model.Dato;
import socketsjee.taller.persistencia.dao.DatoDAO;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Servlet implementation class WebSocketController
 */
@WebServlet("/WebSocketController")
public class WebSocketController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String uri = "ws://127.0.0.1:5051/server.php";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WebSocketController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String opcion = request.getParameter("estado");

		if (opcion.equals("1")) {
			String msg = request.getParameter("msg");

			ArrayList<String> listaLineas = new ArrayList<String>(Arrays.asList(msg.split("\\s*|\\s*")));
			for (String linea : listaLineas) {
				// ARRAY CARGADO CON LOS DATOS EN EL ÓRDEN QUE ESTÁN EN LA BASE
				// DE DATOS DEL SERVER PHP.
				ArrayList<String> datos = new ArrayList<String>(Arrays.asList(linea.split(",")));
				Dato objDato = new Dato();
				objDato.setID(Integer.parseInt(datos.get(0)));
				System.out.println(datos.get(1)); // -----------------
				objDato.setFecha_hora(Date.valueOf(datos.get(1)));
				objDato.setNombreSistema(datos.get(2));
				objDato.setPlataforma(datos.get(3));
				objDato.setSO(datos.get(4));
				objDato.setUsoCPU(Float.parseFloat(datos.get(5)));
				objDato.setUsoRAM(Float.parseFloat(datos.get(6)));

				DatoDAO datoDAO = new DatoDAO();
				try {
					datoDAO.guardarDato(objDato);
					System.out.println("Registro guardado...");
				} catch (SQLException e) {

				}
			}
		}
	}

}
