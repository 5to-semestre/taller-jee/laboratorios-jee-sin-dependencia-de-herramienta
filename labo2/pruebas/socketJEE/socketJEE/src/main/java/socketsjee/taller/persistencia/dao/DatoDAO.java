package socketsjee.taller.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import socketsjee.taller.model.Dato;
import socketsjee.taller.persistencia.conexion.Conexion;

public class DatoDAO {
	private Connection connection;
	private PreparedStatement statement;
	private boolean estadoOperacion;
	//Obtener Conexion pool
	private Connection obtenerConexion() throws SQLException {
		return Conexion.getConnection();
	}
	
	public List<Dato> obtenerDatos() throws SQLException {
		ResultSet resultSet=null;
		String sql=null;
		List<Dato> listaDatos=new ArrayList<>();
		estadoOperacion=false;
		connection=obtenerConexion();
		
		try {
			sql="SELECT * FROM DATOS";
			statement=connection.prepareStatement(sql);
			resultSet=statement.executeQuery(sql);
			while(resultSet.next()) {
				Dato d = new Dato();
				d.setID(Integer.parseInt(resultSet.getString("ID")));
				d.setFecha_hora(resultSet.getDate("fecha_hora"));
				d.setNombreSistema(resultSet.getString("nombreSistema"));
				d.setPlataforma(resultSet.getString("plataforma"));
				d.setSO(resultSet.getString("SO"));
				d.setUsoCPU(Float.parseFloat(resultSet.getString("usoCPU")));
				d.setUsoRAM(Float.parseFloat(resultSet.getString("usoRAM")));
				listaDatos.add(d);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaDatos;
	} 
	
	public boolean guardarDato(Dato dato) throws SQLException {
		String sql = null;
		estadoOperacion = false;
		connection = obtenerConexion();

		try {
			connection.setAutoCommit(false);
			sql = "INSERT INTO `DATOS`(`ID`, `fecha_hora`, `nombreSistema`, `plataforma`, `SO`, `usoCPU`, `usoRAM`) VALUES (?,?,?,?,?,?,?);";
			statement = connection.prepareStatement(sql);

			statement.setString(1, null);
			statement.setDate(2, dato.getFecha_hora());
			statement.setString(3, dato.getNombreSistema());
			statement.setString(4, dato.getPlataforma());
			statement.setString(5, dato.getSO());
			statement.setFloat(6, dato.getUsoCPU());
			statement.setFloat(7, dato.getUsoRAM());
			estadoOperacion = statement.executeUpdate() > 0;

			connection.commit();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			connection.rollback();
			e.printStackTrace();
		}

		return estadoOperacion;
	}
}
