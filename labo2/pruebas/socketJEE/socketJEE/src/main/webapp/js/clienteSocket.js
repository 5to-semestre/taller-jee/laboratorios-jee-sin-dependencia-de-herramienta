function crearCliente(){
	let wsUri = "ws://127.0.0.1:5051/server.php";
	websocket = new WebSocket(wsUri);
	websocket.onopen = function(evt) {
		console.log('[...] ABRIENDO CONEXIÓN');
	};
	websocket.onmessage = function(msg) {
		console.log('[!] MENSAJE RECIBIDO');
		console.log(msg.data); // ------------------
		var datos = {
			"estado" : 1,
			"msg" : msg.data
		};
		var url = "/WebSocketController";
		enviarDatos(datos, url);
		console.log('[...] CERRANDO SOCKET');
		websocket.send("quit");
		websocket.close();
	}
	websocket.onclose = function(msg) {
		console.log('[!] SOCKET CERRADO');
	}
	websocket.onerror = function(msg) {
		console.log('[!!!] ERROR: '+msg.data);
	}
}

function enviarDatos(datos, url) {
	$.ajax({
		data: datos,
		url: url,
		type: 'post',
		succes: function (response) {
			console.log(response);
		},
		error: function (error) {
			console.log(error);
		}
	});
}
